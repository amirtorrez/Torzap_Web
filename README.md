<b>Tachi Web v0.0.0 (Alpha)</b>

Esta es la base del sistema web de Tantra Online desarrollada y mantenida por Amir Torrez. Torzap Web es un _"reboot"_ de Tachi Web.

Este sistema es distribuido bajo la licencia: *CC BY-NC-SA 4.0* (para más información lee el archivo [LICENCIA](https://github.com/amirtorrez/Torzap_Web/blob/master/LICENCIA))
